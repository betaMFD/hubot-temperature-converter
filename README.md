# hubot-temperature-converter

A hubot script that converts F to C and vice versa when people chat.

## Installation

In hubot project repo, run:

`yarn add hubot-temperature-converter --save`

Then add **hubot-temperature-converter** to your `external-scripts.json`:

```json
[
  "hubot-temperature-converter"
]
```


## Setup
There is no setup. Once it's installed and in your external scripts it should just run.
