// Description:
//   convert temperatures
//
// Dependencies:
//   None
//
// Configuration:
//   None
//
// Commands:
//   hubot (digits)F/C


module.exports = function(robot) {
  robot.hear(/(^|\s)\-?\d+\.?\d*(\s|o|°|\*)?[FCK][,.;\?'"]?($|\s)/i, function(msg){
    var input_message = msg.match[0];
    var temp = input_message.match(/\-?\d+\.?\d*.?[FCK]/i)[0];
    thenumber = temp.match(/\-?\d+\.?\d*/)[0]
    thetype = temp.match(/[FCK]/i)[0]
    if (thetype == 'F' || thetype == 'f') {
      var celcius = (thenumber - 32) * 5/9;
      celcius = Math.round((celcius + Number.EPSILON) * 100) / 100
        msg.send(thenumber + '°F is ' + celcius + '°C');
    } else if (thetype == 'C' || thetype == 'c') {
      var fahrenheight = (thenumber * 9/5) + 32;
      fahrenheight = Math.round((fahrenheight + Number.EPSILON) * 100) / 100
        msg.send(thenumber + '°C is ' + fahrenheight + '°F');
    } else if (thetype == 'K' || thetype == 'k') {
      //because every smartass who sees this script run tries kelvin
      var celcius = thenumber - 273.15;
      celcius = Math.round((celcius + Number.EPSILON) * 100) / 100
      var fahrenheight = (thenumber - 273.15) * 9 / 5 + 32;
      fahrenheight = Math.round((fahrenheight + Number.EPSILON) * 100) / 100
        msg.send(thenumber + 'K is ' + fahrenheight + '°F or ' + celcius + '°C');
    }
  });
}
